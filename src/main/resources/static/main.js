

let getData = function() {

    let http = new XMLHttpRequest();

    http.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE) {
            if (this.status === 200) {
                document.getElementById("result").innerHTML = http.responseText;
                document.getElementById("error").innerHTML = "";
            } else {
                document.getElementById("result").innerHTML = "";
                document.getElementById("error").innerHTML = JSON.parse(http.responseText).message;
            }
        }
    };

    let inputElement = document.getElementById("name");
    let selectElement = document.getElementById("letterCase");

    let request = {
        name: inputElement.value,
        letterCase: selectElement.options[selectElement.selectedIndex].value
    }

    let data = JSON.stringify(request);
    console.log("Request", request);

    http.open("POST", "/convert", true);
    http.setRequestHeader("Content-type", "application/json");
    http.send(data);

};
