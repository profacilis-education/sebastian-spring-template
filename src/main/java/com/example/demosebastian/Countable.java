package com.example.demosebastian;

public interface Countable {

	String getSubString(String text);

}
