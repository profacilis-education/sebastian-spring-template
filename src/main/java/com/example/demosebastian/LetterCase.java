package com.example.demosebastian;

public enum LetterCase {

	LOWER, UPPER, AS_IS

}
