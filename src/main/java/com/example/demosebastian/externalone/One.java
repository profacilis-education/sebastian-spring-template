package com.example.demosebastian.externalone;


import com.example.demosebastian.Countable;
import com.example.demosebastian.Top;

public class One extends Top implements Countable {

	private final int stringStart;
	private final int stringLength;

	public One(int stringStart, int stringLength) {
		this.stringStart = stringStart;
		this.stringLength = stringLength;
	}

	@Override
	protected String getName() {
		return "One";
	}

	@Override
	public String getSubString(String text) {
		return text.substring(stringStart, stringStart + stringLength);
	}
}
