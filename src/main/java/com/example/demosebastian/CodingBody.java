package com.example.demosebastian;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class CodingBody implements CommandLineRunner {

	@Override
	public void run(String... args) throws Exception {
		System.out.println("After Spring started");
	}

}
