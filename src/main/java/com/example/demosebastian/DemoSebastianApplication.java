package com.example.demosebastian;

import com.example.demosebastian.compare.StringCompare;
import com.example.demosebastian.compare.TestCompare;
import com.example.demosebastian.externalone.One;
import com.example.demosebastian.externaltwo.Two;

import java.util.*;

/**
 * uncomment annotation below to get spring boot working
 */

// @SpringBootApplication
public class DemoSebastianApplication {

	private static final String TEXT = "Żółta gęsia jaźń";
	private static final int TAB_SIZE = 1000;

	public static void main(String[] args) {

		// Locale.setDefault(Locale.US);
		Locale.setDefault(new Locale("pl", "PL"));

		// firstSteps();

		sortAsJavaImplemented();
		sortBubble();

		/* uncomment line below to get spring boot working */
		// SpringApplication.run(DemoSebastianApplication.class, args);

	}

	private static void sortBubble() {
		ArrayList<Integer> input = new ArrayList<>();


		Random random = new Random();
		for (int i = 0; i < TAB_SIZE; i++) {
			input.add(random.nextInt());
		}

		Integer[] tab = input.toArray(new Integer[0]);
		Date start = new Date();

		int temp;
		int zmiana = 1;
		while(zmiana > 0){
			zmiana = 0;
			for(int i=0; i<tab.length-1; i++){
				if(tab[i]>tab[i+1]){
					temp = tab[i+1];
					tab[i+1] = tab[i];
					tab[i] = temp;
					zmiana++;
				}
			}
		}

		Date stop = new Date();

		long startMill = start.getTime();
		long stopMill = stop.getTime();

		System.out.println("Bubble " + (stopMill - startMill));
	}

	private static void sortAsJavaImplemented() {
		ArrayList<Integer> input = new ArrayList<>();
		Random random = new Random();
		for (int i = 0; i < TAB_SIZE; i++) {
			input.add(random.nextInt());
		}

		for (Integer i : input) {
			System.out.println(i);
		}

		Date start = new Date();
		Collections.sort(input);
		Date stop = new Date();

		long startMill = start.getTime();
		long stopMill = stop.getTime();

		System.out.println("Java Core " + (stopMill - startMill));

	}

	private static void firstSteps() {
		doMethods(new One(2, 8));
		doMethods(new Two());

		List<TestCompare> testCompares = Arrays.asList(
				new TestCompare(4, 6),
				new TestCompare(3, 7),
				new TestCompare(3, 5),
				new TestCompare(8, 8),
				new TestCompare(6, 7),
				new TestCompare(1, 9),
				new TestCompare(5, 8),
				new TestCompare(2, 6)
		);

		final String firstFull = "ładny";
		final String secondFull = "matka";

		StringCompare first = new StringCompare(firstFull);
		StringCompare second = new StringCompare(secondFull);

		int compare = first.compareTo(second);
		if (compare > 0) {
			System.out.printf("First string %s goes first", firstFull);
		} else if (compare < 0) {
			System.out.printf("Second string %s goes first", secondFull);
		} else {
			System.out.print("Strings are equal");
		}
		System.out.println();

		System.out.println("Before sorting...:");
		printList(testCompares);

		System.out.println("Sorting...:");
		Collections.sort(testCompares);

		System.out.println("After Sort...:");
		printList(testCompares);

		System.out.println();
	}

	private static void printList(List<TestCompare> list) {
		for (TestCompare testCompare: list) {
			System.out.printf("%2d * %2d = %2d\n", testCompare.getField1(), testCompare.getField2(), testCompare.getField1() * testCompare.getField2());
		}
	}

	private static void doMethods(Top top) {
		System.out.printf("I'm of type %s and my name is %s", top.getClass().getSimpleName(), top.getName());
		System.out.println();
		if (top instanceof Countable) {
			Countable countableTop = (Countable) top;
			System.out.printf("I'm of type %s and my string is %s", top.getClass().getSimpleName(), countableTop.getSubString(TEXT));
		} else {
			System.out.printf("Sorry, I'm of type %s and I'm not Countable", top.getClass().getSimpleName());
		}
		System.out.println();
	}



}
