package com.example.demosebastian;

import lombok.Data;

@Data
public class Request {

	private String name;
	private LetterCase letterCase;

}
