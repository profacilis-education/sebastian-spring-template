package com.example.demosebastian;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;


@RestController
public class TestController {

	@PostMapping(value = "/convert", produces = "text/plain")
	public String nameConversion(@RequestBody Request request) {
		String input = request.getName();
		int size = input.length();
		if (size > 10) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("String size %d, too long, 10 characters max", size));
		}
		switch (request.getLetterCase()) {
			case LOWER:
				return input.toLowerCase();
			case UPPER:
				return input.toUpperCase();
			default:
				return input;
		}
	}

}
