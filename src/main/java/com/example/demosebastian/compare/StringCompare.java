package com.example.demosebastian.compare;

public class StringCompare implements Comparable<StringCompare> {

	private String myString;

	public StringCompare(String myString) {
		this.myString = myString;
	}

	public String getMyString() {
		return myString;
	}

	public void setMyString(String myString) {
		this.myString = myString;
	}

	@Override
	public int compareTo(StringCompare o) {
		String mySub = myString.substring(0, 3);
		String otherSub = o.getMyString().substring(0, 3);
		return otherSub.compareTo(mySub);
	}
}
