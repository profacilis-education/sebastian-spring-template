package com.example.demosebastian.compare;

import lombok.Data;

@Data
public class TestCompare implements Comparable<TestCompare> {

	private int field1;
	private int field2;

	public TestCompare(int field1, int field2) {
		this.field1 = field1;
		this.field2 = field2;
	}

	@Override
	public int compareTo(TestCompare o) {
		int thisValue = field1 * field2;
		int thatValue = o.field1 * o.field2;
		if (thisValue == thatValue) return 0;
		return thisValue > thatValue ? 1 : -1;
	}

}
